import java.util.Arrays;

boolean inputGiven;
boolean mouseClicked;
String[] questions;
int questionNumber = -1;
int phase = 0;

Button okButton;
boolean[] answers;
PImage img;
PFont myFont;
String msg;
Person[] currentPeople = new Person[8];
Person chosenPerson;


void keyPressed()
{
  inputGiven = true;
}
void mouseClicked() 
{
  mouseClicked = true;
}
void setup()
{
   size(1500,1500);
   img = loadImage("WerIstEsKarten2.jpg");
   myFont = createFont("Terminess.ttf", 31);
   textFont(myFont);
   textAlign(CENTER,CENTER);
   questions = new String[]
   {
    "Is the person blonde?",
    "Does the person wear glasses?",
    "Does the person have beard?",
    "Doese the person wear sun glasses?"
   }; 
  
  peopleInitializer();
  answers = new boolean[questions.length];
  inputGiven = false;
  msg = "Choose and remember one of these following persons: ";
  okButton = new Button(width/2, 650, 100, 60, 10, "OK");
}

void draw() 
{ 
  //Gameloop
  background(152,251,152);
  drawFaces();
  drawTopMsg(msg);
 /*---- PHASE 0: Welcome screen ----*/
  if(phase == 0)
  {
    okButton.drawButton();
    if(inputGiven)
    {
      checkForInput();
    }
  } 
  else if(phase == 1 && questionNumber < questions.length-1)
  { /*---- PHASE 1: Question phase ----*/   
    questionNumber++;
    msg = questions[questionNumber];
    drawBottomMsg("Press y for YES or n for NO");
    phase++;    
  } 
 else if(phase == 2)
 { /*---- PHASE 2: Answer phase ----*/
    drawBottomMsg("Press y for YES or n for NO");

    if(inputGiven)
    {
      checkForInput();
      if(questionNumber == questions.length-1) phase = 3;
    }
  } 
  else if(phase == 3)
  { /*---- PHASE 3: Result phase ----*/
    // select a person after requesting answers to all three questions and display the name
    choosePerson();
    msg = "It's " + chosenPerson.getName();
    phase++;
  } 
  else if(phase == 4)
  { /*---- PHASE 4: Game over screen ----*/
    drawBottomMsg("Press r to restart or q to quit");
    if(inputGiven)
    {
      checkForInput();
    }
  }  
  mouseClicked = false;
}

void drawFaces()
{
  imageMode(CENTER);
  image(img, width/2, 400);
}

void drawTopMsg(String _msg)
{
  textAlign(CENTER);
  textSize(31);
  text(_msg, width/2, 200);
}

void drawBottomMsg(String _msg)
{
  text(_msg, width/2, 750);
}

class Button
{
  private int x;
  private int y;
  private int width;
  private int height;
  private int radius;
  private String text;

  Button(int _x, int _y, int _width, int _height, int _radius, String _text)
  {
    this.x = _x;
    this.y = _y;
    this.width = _width;
    this.height = _height;
    this.radius = _radius;
    this.text = _text;
  }

  void drawButton()
  {
    if(mouseClicked && (mouseX < x + width/2 && mouseX > x - width/2) && (mouseY < y + height/2 && mouseY > y - height/2))
    {
      println("Button pressed");
      phase++;
    }
    if(phase == 0)
    {
      fill(150);
      rectMode(CENTER);
      rect(x,y,width,height,radius);
      fill(0);
      textAlign(CENTER, CENTER);
      myFont = createFont("Terminess.ttf",36);
      textFont(myFont);      
      text(text, x, y-3);
    }
  }
}

/**
 * This method:
 * - saves the given answer in the answers array
 * - resets the phase to 1 if the answer was given
 * - restarts the game when the player presses 'r' 
 */

void checkForInput()
{
  switch(key)
  {
      case 'y':
        if(phase == 2)
        {
          answers[questionNumber] = true;
          phase = 1;
        }
        break;
      case 'n':
        if(phase == 2)
        {
          answers[questionNumber] = false;
          phase = 1;
        }
        break;
      case 'r':
          phase = 0;
          msg = "Choose and remember one of the following persons:";
          questionNumber = -1;
          answers = new boolean[questions.length];
        break;
      case 'q':
        exit();
        break;
      default:
        break;
  }
    inputGiven = false;
}

/*
* This method fills the array of people with content.
*/
void peopleInitializer()
{
    currentPeople = new Person[8];
    // Person(String name, boolean hair, boolean beard, boolean glasses, boolean sunglasses) 
    currentPeople[0] = new Person ("Armin", true, true, true, true);
    currentPeople[1] = new Person ("Bert", true, false, true, false);
    currentPeople[2] = new Person ("Claus", false, true, true, false);
    currentPeople[3] = new Person ("Detlef", false, false, true, false);
    currentPeople[4] = new Person ("Emil", true, true, false,false);
    currentPeople[5] = new Person ("Felix", true, false, false, false);
    currentPeople[6] = new Person ("Gustav", false, true, false,false);
    currentPeople[7] = new Person ("Henrik", false, false, false,false);
}

/*
* This method chooses the person based on the given answers.
*/
void choosePerson() 
{
// loop over the existing people to find the person that matches the given answers
  for (int i = 0; i < currentPeople.length; i++)
  {
    Person currPerson = currentPeople[i];
    if (currPerson.getHaircolor() == answers[0] && currPerson.getGlasses() == answers[1] && currPerson.getBeard() == answers[2]) 
    {
      chosenPerson = currentPeople[i];
      break;
    } 
  }
}

// people query
void askQuestion(String q)
{
  println(q);
}

/**
 * Class to add the People to our game easier
 */
 
class Person 
{
  private String name = "";
  private boolean haircolor = false;
  private boolean hasBeard = false;
  private boolean hasGlasses = false;
  private boolean hasSunGlasses = false;
  
   /*
   * Constructor of this class
   * @param name the name of this person
   * @param the haircolor of this person (true-> blonde, false -> black)
   * @param beard whether this person has beard
   * @param glasses whether this person wears glasses
   * @param sunglasses whether this person wears sun glasses
   */
  Person(String name, boolean hair, boolean beard, boolean glasses, boolean sunglasses) 
  {
    this.name = name;
    this.haircolor = hair;
    this.hasBeard = beard;
    this.hasGlasses = glasses;
    this.hasSunGlasses = sunglasses;
  }
  
  /*
  * Getter: Name
  * @return the name of this person
  */
  String getName () 
  {
    return this.name;
  }
  
  /*
  * Getter: haircolor
  * @return true if haircolor is blonde, false if black
  */
  boolean getHaircolor () 
  {
    return this.haircolor;
  }
  
  /*
  * Getter: beard
  * @return true if person has a beard, false if not
  */
  boolean getBeard () 
  {
    return this.hasBeard;
  }
  
  /*
  * Getter: glasses
  * @return true if person has glasses, false if not
  */
  boolean getGlasses () 
  {
    return this.hasGlasses;
  }
   /*
  * Getter: glasses
  * @return true if person has glasses, false if not
  */
  boolean getSunGlasses()
  {
    return this.hasSunGlasses;
  }
 
}
